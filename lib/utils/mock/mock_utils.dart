import 'dart:core';
import 'package:Criluma_Nozze/models/criluma/mock_offerta.dart';
import 'package:intl/intl.dart';

class MockUtils {


  static List<MockObject> getMockDestinazioni() {
    var result = new List<MockObject>();
    result.add(new MockObject("Qualsiasi", 1));
    result.add(new MockObject("Emirati Arabi Uniti", 2));
    result.add(new MockObject("Mar Rosso", 3));
    return result;

  }

  static List<MockObject> getMockPartenzaDa() {
    var result = new List<MockObject>();
    result.add(new MockObject("Qualsiasi", 1));
    result.add(new MockObject("Aeroporto di Venezia-Tessera (Italia)", 2));
    result.add(new MockObject("Bari - Palese	(Italia)", 3));
    result.add(new MockObject("Bergamo - Orio al Serio	(Italia)", 4));
    result.add(new MockObject("Bologna - Guglielmo Marconi(Italia)", 5));
    result.add(new MockObject("Milano - Malpensa	(Italia)", 6));
    result.add(new MockObject("Napoli - Capodichino	(Italia)", 7));
    result.add(new MockObject("Nice - Nice ( France)", 8));
    result.add(new MockObject("Roma - Roma Fiumicino ( Italy)", 9));
    result.add(new MockObject("Verona-Villafranca (Italia)", 10));

    return result;
  }

//  static List<MockOfferta> getMockOfferte(){
//
//    var result = new List<MockOfferta>();
//    result.add(new MockOfferta("ANTIGUA E BARBUDA", "Saint John's", "2600", 2, "EDEN VILLAGE PREMIUM OCEAN POINT", DateTime(2018, 8, 25), DateTime(2018, 9, 1),
//        "http://217.61.125.146:83//Images/Offerte/image_andrea_2019_03_19_11_08_20_test-off.jpg",3));
//    result.add(new MockOfferta("CROAZIA", "Isola di Pag", "370", 1, "APPARTAMENTI PRIVATI", DateTime(2018, 8, 25), DateTime(2018, 9, 1),
//        "http://217.61.125.146:83//Images/Offerte/image_andrea_2019_03_19_11_08_48_test-off-2.jpg", null));
//    result.add(new MockOfferta("ANTIGUA E BARBUDA", "Saint John's", "1700", 2, "EDEN VILLAGE PREMIUM OCEAN POINT", DateTime(2018, 8, 25), DateTime(2018, 9, 1),
//        "http://217.61.125.146:83//Images/Offerte/image_andrea_2019_03_19_11_09_15_test-off-3.jpg",3));
//    result.add(new MockOfferta("ZANZIBAR", "Kendwa", "3200", 2, "EDEN VILLAGE PREMIUM KENDWA", DateTime(2018, 8, 28), DateTime(2018, 9, 4),
//        "http://217.61.125.146:83//Images/Offerte/image_andrea_2019_03_19_11_09_43_test-off-4.jpg",4));
//
//    return result;
//  }

//  static List<CrilumaDropdownItem> getDestinazioniDepth1(){
//    var result = new List<CrilumaDropdownItem>();
//    result.add(new CrilumaDropdownItem(1, "Qualsiasi", true));
//    result.add(new CrilumaDropdownItem(2, "Antigua e Barbuda", false));
//    result.add(new CrilumaDropdownItem(3, "Baleari", false));
//    result.add(new CrilumaDropdownItem(23, "Canarie", false));
//    result.add(new CrilumaDropdownItem(4, "Cipro", false));
//    result.add(new CrilumaDropdownItem(5, "Cupa", false));
//    result.add(new CrilumaDropdownItem(6, "Emirati Arabi Uniti", false));
//    result.add(new CrilumaDropdownItem(7, "Giamaica", false));
//    result.add(new CrilumaDropdownItem(8, "Grecia", false));
//    result.add(new CrilumaDropdownItem(9, "Italia", false));
//    result.add(new CrilumaDropdownItem(10, "Kenya", false));
//    result.add(new CrilumaDropdownItem(11, "Madagascar", false));
//    result.add(new CrilumaDropdownItem(12, "Maldive", false));
//    result.add(new CrilumaDropdownItem(13, "Malta", false));
//    result.add(new CrilumaDropdownItem(14, "Mar Rosso", false));
//    result.add(new CrilumaDropdownItem(15, "Marocco", false));
//    result.add(new CrilumaDropdownItem(16, "Messico", false));
//    result.add(new CrilumaDropdownItem(17, "Oman", false));
//    result.add(new CrilumaDropdownItem(18, "Portogallo", false));
//    result.add(new CrilumaDropdownItem(19, "Repubblica Dominicana", false));
//    result.add(new CrilumaDropdownItem(20, "Spagna", false));
//    result.add(new CrilumaDropdownItem(21, "Tunisia", false));
//    result.add(new CrilumaDropdownItem(22, "Zanzibar", false));
//    return result;
//  }
//
//  static List<CrilumaDropdownItem> getAereoportiDepth1(){
//    var result = new List<CrilumaDropdownItem>();
//    result.add(new CrilumaDropdownItem(1, "Qualsiasi", true));
//    result.add(new CrilumaDropdownItem(23, "Aeroporto di Venezia-Tessera (Italia)", false));
//    result.add(new CrilumaDropdownItem(2, "Ancona - Falconara(Italia)", false));
//    result.add(new CrilumaDropdownItem(3, "Bari - Palese	(Italia)", false));
//    result.add(new CrilumaDropdownItem(4, "Bergamo - Orio al Serio	(Italia)", false));
//    result.add(new CrilumaDropdownItem(5, "Bologna - Guglielmo Marconi(Italia)", false));
//    result.add(new CrilumaDropdownItem(6, "Catania - Fontanarossa	(Italia)", false));
//    result.add(new CrilumaDropdownItem(7, "Firenze - Amerigo Vespucci	(Italia)", false));
//    result.add(new CrilumaDropdownItem(8, "Genova - Cristoforo Colombo (Italia)", false));
//    result.add(new CrilumaDropdownItem(9, "Lamezia Terme - Lamezia Terme (Italia)", false));
//    result.add(new CrilumaDropdownItem(10, "Milano - Linate(Italia)", false));
//    result.add(new CrilumaDropdownItem(11, "Milano - Malpensa	(Italia)", false));
//    result.add(new CrilumaDropdownItem(12, "Napoli - Capodichino	(Italia)", false));
//    result.add(new CrilumaDropdownItem(13, "Nice - Nice ( France)", false));
//    result.add(new CrilumaDropdownItem(14, "Palermo - Palermo ( Sicily, Italy)", false));
//    result.add(new CrilumaDropdownItem(15, "Perugia - S.Egidio(Italia)", false));
//    result.add(new CrilumaDropdownItem(16, "Pescara - Pescara (Italia)", false));
//    result.add(new CrilumaDropdownItem(17, "Pisa - Galileo Galilei	(Italia)", false));
//    result.add(new CrilumaDropdownItem(18, "Rimini - Rimini (Italia)", false));
//    result.add(new CrilumaDropdownItem(19, "Roma - Roma Fiumicino ( Italy)", false));
//    result.add(new CrilumaDropdownItem(20, "Torino - Torino-Caselle - Italia", false));
//    result.add(new CrilumaDropdownItem(21, "Treviso - Treviso (Italia)", false));
//    result.add(new CrilumaDropdownItem(22, "Verona-Villafranca (Italia)", false));
//    return result;
//  }



}




class MockObject{

  MockObject(this.text, this.value);

  String text;
  int value;
}




import 'dart:developer';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const debugNetwork = false;
const debugPC03 = false;

const greenCriluma = Color(0xFF0E693E); //0xFF è solo per le trasparenze

const MaterialColor blueCriluma = MaterialColor(
  0xFF5d9fd7,
  <int, Color>{
    50: Color(0xFFe6f0f9),
    100: Color(0xFFaecfeb),
    200: Color(0xFF8dbbe3),
    300: Color(0xFF7db2df),
    400: Color(0xFF6da8db),
    500: Color(0xFF5d9fd7),
    600: Color(0xFF538fc1),
    700: Color(0xFF416f96),
    800: Color(0xFF2e4f6b),
    900: Color(0xFF1b2f40),
  },
);

const facebookColor = const Color(0xff3C5A99);
const googleSignColor = const Color(0xffDB4437);

//TEST
//const BASE_URL = "http://217.61.125.146/CrilumaWebProvider_Test";
//const BASE_URL = "https://www.crilumaviaggitest.it/WebProviderTest";

//PRDO
//const BASE_URL = "http://217.61.125.146/CrilumaWebProvider";
const BASE_URL = "https://www.crilumaviaggi.com/WebProvider";


const URL_SITO = "https://www.crilumaviaggitest.it/it/";

class CrilumaStaticValues{

  static bool isOpen() {
    DateTime now = new DateTime.now();
    return now.hour > 8 && now.hour < 19 && now.weekday != DateTime.sunday;
  }


}

final dateTimeParseServer = DateFormat("yyyy-MM-dd'T'HH:mm:ss");
final dateTimeParseSimple = DateFormat("yyyy-MM-dd HH.mm");
final dateParse = DateFormat("yyyy-MM-dd");

final dateFormat = DateFormat("dd/MM/yyyy");

final currencyFormat = NumberFormat("#,##0.00", "de_DE");


const String PAGINA_HOME = "Home";
const String PAGINA_CONTATTI = "Contatti";
const String PAGINA_RICERCA = "Ricerca";

//2018-10-31T00:00:00


import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FontAwesomeHelper {


  static IconData parseString(String htmlAttribute){

    IconData result;

    if(htmlAttribute.contains("fa-cut"))
    {result = FontAwesomeIcons.cut;}

    if(htmlAttribute.contains("fa-cutlery"))
    {result = FontAwesomeIcons.utensils;}

    if(htmlAttribute.contains("fa-glass"))
    {result = FontAwesomeIcons.glassMartini;}

    if(htmlAttribute.contains("fa-music"))
    {result = FontAwesomeIcons.music;}

    if(htmlAttribute.contains("fa-search"))
    {result = FontAwesomeIcons.search;}

    if(htmlAttribute.contains("fa-envelope-o"))
    {result = FontAwesomeIcons.envelope;}

    if(htmlAttribute.contains("fa-heart"))
    {result = FontAwesomeIcons.heart;}

    if(htmlAttribute.contains("fa-star"))
    {result = FontAwesomeIcons.star;}

    if(htmlAttribute.contains("fa-star"))
    {result = FontAwesomeIcons.star;}

    if(htmlAttribute.contains("fa-user"))
    {result = FontAwesomeIcons.user;}

    if(htmlAttribute.contains("fa-film"))
    {result = FontAwesomeIcons.film;}

    if(htmlAttribute.contains("fa-th-large"))
    {result = FontAwesomeIcons.th;}

    if(htmlAttribute.contains("fa-th"))
    {result = FontAwesomeIcons.th;}

    if(htmlAttribute.contains("fa-th-list"))
    {result = FontAwesomeIcons.th;}

    if(htmlAttribute.contains("fa-check"))
    {result = FontAwesomeIcons.check;}

    if(htmlAttribute.contains("fa-times"))
    {result = FontAwesomeIcons.times;}

    if(htmlAttribute.contains("fa-search-plus"))
    {result = FontAwesomeIcons.search;}

    if(htmlAttribute.contains("fa-search-minus"))
    {result = FontAwesomeIcons.search;}

    if(htmlAttribute.contains("fa-power-off"))
    {result = FontAwesomeIcons.powerOff;}

    if(htmlAttribute.contains("fa-signal"))
    {result = FontAwesomeIcons.signal;}

    if(htmlAttribute.contains("fa-cog"))
    {result = FontAwesomeIcons.cog;}

    if(htmlAttribute.contains("fa-trash-o"))
    {result = FontAwesomeIcons.trash;}

    if(htmlAttribute.contains("fa-home"))
    {result = FontAwesomeIcons.home;}

    if(htmlAttribute.contains("fa-file-o"))
    {result = FontAwesomeIcons.file;}

    if(htmlAttribute.contains("fa-clock-o"))
    {result = FontAwesomeIcons.clock;}

    if(htmlAttribute.contains("fa-road"))
    {result = FontAwesomeIcons.road;}

    if(htmlAttribute.contains("fa-download"))
    {result = FontAwesomeIcons.download;}

    if(htmlAttribute.contains("fa-arrow-circle-o-down"))
    {result = FontAwesomeIcons.arrowDown;}

    if(htmlAttribute.contains("fa-arrow-circle-o-up"))
    {result = FontAwesomeIcons.arrowCircleUp;}

    if(htmlAttribute.contains("fa-inbox"))
    {result = FontAwesomeIcons.inbox;}

    if(htmlAttribute.contains("fa-play-circle-o"))
    {result = FontAwesomeIcons.play;}

    if(htmlAttribute.contains("fa-repeat"))
    {result = FontAwesomeIcons.redo;}

    if(htmlAttribute.contains("fa-sync"))
    {result = FontAwesomeIcons.sync;}

    if(htmlAttribute.contains("fa-list-alt"))
    {result = FontAwesomeIcons.list;}

    if(htmlAttribute.contains("fa-lock"))
    {result = FontAwesomeIcons.lock;}

    if(htmlAttribute.contains("fa-flag"))
    {result = FontAwesomeIcons.flag;}

    if(htmlAttribute.contains("fa-headphones"))
    {result = FontAwesomeIcons.headphones;}

    if(htmlAttribute.contains("fa-volume-off"))
    {result = FontAwesomeIcons.volumeOff;}

    if(htmlAttribute.contains("fa-volume-down"))
    {result = FontAwesomeIcons.volumeDown;}

    if(htmlAttribute.contains("fa-volume-up"))
    {result = FontAwesomeIcons.volumeUp;}

    if(htmlAttribute.contains("fa-qrcode"))
    {result = FontAwesomeIcons.qrcode;}

    if(htmlAttribute.contains("fa-barcode"))
    {result = FontAwesomeIcons.barcode;}

    if(htmlAttribute.contains("fa-tag"))
    {result = FontAwesomeIcons.tag;}

    if(htmlAttribute.contains("fa-tags"))
    {result = FontAwesomeIcons.tags;}

    if(htmlAttribute.contains("fa-book"))
    {result = FontAwesomeIcons.book;}

    if(htmlAttribute.contains("fa-bookmark"))
    {result = FontAwesomeIcons.bookmark;}

    if(htmlAttribute.contains("fa-print"))
    {result = FontAwesomeIcons.print;}

    if(htmlAttribute.contains("fa-camera"))
    {result = FontAwesomeIcons.camera;}

    if(htmlAttribute.contains("fa-font"))
    {result = FontAwesomeIcons.font;}

    if(htmlAttribute.contains("fa-bold"))
    {result = FontAwesomeIcons.bold;}

    if(htmlAttribute.contains("fa-italic"))
    {result = FontAwesomeIcons.italic;}

    if(htmlAttribute.contains("fa-text-height"))
    {result = FontAwesomeIcons.textHeight;}

    if(htmlAttribute.contains("fa-text-width"))
    {result = FontAwesomeIcons.textWidth;}

    if(htmlAttribute.contains("fa-align-left"))
    {result = FontAwesomeIcons.alignLeft;}

    if(htmlAttribute.contains("fa-align-center"))
    {result = FontAwesomeIcons.alignCenter;}

    if(htmlAttribute.contains("fa-align-right"))
    {result = FontAwesomeIcons.alignRight;}

    if(htmlAttribute.contains("fa-align-justify"))
    {result = FontAwesomeIcons.alignJustify;}

    if(htmlAttribute.contains("fa-list"))
    {result = FontAwesomeIcons.list;}

    if(htmlAttribute.contains("fa-outdent"))
    {result = FontAwesomeIcons.outdent;}

    if(htmlAttribute.contains("fa-indent"))
    {result = FontAwesomeIcons.indent;}

    if(htmlAttribute.contains("fa-video-camera"))
    {result = FontAwesomeIcons.video;}

    if(htmlAttribute.contains("fa-image"))
    {result = FontAwesomeIcons.image;}

    if(htmlAttribute.contains("fa-pencil-alt"))
    {result = FontAwesomeIcons.pencilAlt;}

    if(htmlAttribute.contains("fa-pen"))
    {result = FontAwesomeIcons.pen;}

    if(htmlAttribute.contains("fa-map-marker"))
    {result = FontAwesomeIcons.map;}

    if(htmlAttribute.contains("fa-adjust"))
    {result = FontAwesomeIcons.adjust;}

    if(htmlAttribute.contains("fa-tint"))
    {result = FontAwesomeIcons.tint;}

    if(htmlAttribute.contains("fa-edit"))
    {result = FontAwesomeIcons.edit;}

    if(htmlAttribute.contains("fa-share-square-o"))
    {result = FontAwesomeIcons.shareSquare;}

    if(htmlAttribute.contains("fa-check-square-o"))
    {result = FontAwesomeIcons.checkSquare;}

    if(htmlAttribute.contains("fa-arrows-alt"))
    {result = FontAwesomeIcons.arrowsAlt;}

    if(htmlAttribute.contains("fa-step-backward"))
    {result = FontAwesomeIcons.stepBackward;}

    if(htmlAttribute.contains("fa-fast-backward"))
    {result = FontAwesomeIcons.fastBackward;}

    if(htmlAttribute.contains("fa-backward"))
    {result = FontAwesomeIcons.backward;}

    if(htmlAttribute.contains("fa-play"))
    {result = FontAwesomeIcons.play;}

    if(htmlAttribute.contains("fa-pause"))
    {result = FontAwesomeIcons.pause;}

    if(htmlAttribute.contains("fa-stop"))
    {result = FontAwesomeIcons.stop;}

    if(htmlAttribute.contains("fa-forward"))
    {result = FontAwesomeIcons.forward;}

    if(htmlAttribute.contains("fa-fast-forward"))
    {result = FontAwesomeIcons.fastForward;}

    if(htmlAttribute.contains("fa-step-forward"))
    {result = FontAwesomeIcons.stepForward;}

    if(htmlAttribute.contains("fa-eject"))
    {result = FontAwesomeIcons.eject;}

    if(htmlAttribute.contains("fa-chevron-left"))
    {result = FontAwesomeIcons.chevronLeft;}

    if(htmlAttribute.contains("fa-chevron-right"))
    {result = FontAwesomeIcons.chevronRight;}

    if(htmlAttribute.contains("fa-plus-circle"))
    {result = FontAwesomeIcons.plusCircle;}

    if(htmlAttribute.contains("fa-minus-circle"))
    {result = FontAwesomeIcons.minusCircle;}

    if(htmlAttribute.contains("fa-times-circle"))
    {result = FontAwesomeIcons.timesCircle;}

    if(htmlAttribute.contains("fa-check-circle"))
    {result = FontAwesomeIcons.checkCircle;}

    if(htmlAttribute.contains("fa-question-circle"))
    {result = FontAwesomeIcons.questionCircle;}

    if(htmlAttribute.contains("fa-info-circle"))
    {result = FontAwesomeIcons.infoCircle;}

    if(htmlAttribute.contains("fa-crosshairs"))
    {result = FontAwesomeIcons.crosshairs;}

    if(htmlAttribute.contains("fa-times-circle-o"))
    {result = FontAwesomeIcons.timesCircle;}

    if(htmlAttribute.contains("fa-check-circle-o"))
    {result = FontAwesomeIcons.checkCircle;}

    if(htmlAttribute.contains("fa-ban"))
    {result = FontAwesomeIcons.ban;}

    if(htmlAttribute.contains("fa-arrow-left"))
    {result = FontAwesomeIcons.arrowLeft;}

    if(htmlAttribute.contains("fa-arrow-right"))
    {result = FontAwesomeIcons.arrowRight;}

    if(htmlAttribute.contains("fa-arrow-up"))
    {result = FontAwesomeIcons.arrowUp;}

    if(htmlAttribute.contains("fa-arrow-down"))
    {result = FontAwesomeIcons.arrowDown;}

    if(htmlAttribute.contains("fa-share"))
    {result = FontAwesomeIcons.share;}

    if(htmlAttribute.contains("fa-expand"))
    {result = FontAwesomeIcons.expand;}

    if(htmlAttribute.contains("fa-compress"))
    {result = FontAwesomeIcons.compress;}

    if(htmlAttribute.contains("fa-plus"))
    {result = FontAwesomeIcons.plus;}

    if(htmlAttribute.contains("fa-minus"))
    {result = FontAwesomeIcons.minus;}

    if(htmlAttribute.contains("fa-asterisk"))
    {result = FontAwesomeIcons.asterisk;}

    if(htmlAttribute.contains("fa-exclamation-circle"))
    {result = FontAwesomeIcons.exclamationCircle;}

    if(htmlAttribute.contains("fa-gift"))
    {result = FontAwesomeIcons.gift;}

    if(htmlAttribute.contains("fa-leaf"))
    {result = FontAwesomeIcons.leaf;}

    if(htmlAttribute.contains("fa-fire"))
    {result = FontAwesomeIcons.fire;}

    if(htmlAttribute.contains("fa-eye"))
    {result = FontAwesomeIcons.eye;}

    if(htmlAttribute.contains("fa-eye-slash"))
    {result = FontAwesomeIcons.eye;}

    if(htmlAttribute.contains("fa-exclamation-triangle"))
    {result = FontAwesomeIcons.exclamationTriangle;}

    if(htmlAttribute.contains("fa-plane"))
    {result = FontAwesomeIcons.plane;}

    if(htmlAttribute.contains("fa-calendar"))
    {result = FontAwesomeIcons.calendar;}

    if(htmlAttribute.contains("fa-random"))
    {result = FontAwesomeIcons.random;}

    if(htmlAttribute.contains("fa-comment"))
    {result = FontAwesomeIcons.comment;}

    if(htmlAttribute.contains("fa-magnet"))
    {result = FontAwesomeIcons.magnet;}

    if(htmlAttribute.contains("fa-chevron-up"))
    {result = FontAwesomeIcons.chevronUp;}

    if(htmlAttribute.contains("fa-chevron-down"))
    {result = FontAwesomeIcons.chevronDown;}

    if(htmlAttribute.contains("fa-retweet"))
    {result = FontAwesomeIcons.retweet;}

    if(htmlAttribute.contains("fa-shopping-cart"))
    {result = FontAwesomeIcons.shoppingCart;}

    if(htmlAttribute.contains("fa-folder"))
    {result = FontAwesomeIcons.folder;}

    if(htmlAttribute.contains("fa-folder-open"))
    {result = FontAwesomeIcons.folder;}

    if(htmlAttribute.contains("fa-arrows-alt-v"))
    {result = FontAwesomeIcons.arrowsAltV;}

    if(htmlAttribute.contains("fa-arrows-alt-h"))
    {result = FontAwesomeIcons.arrowsAltH;}

    if(htmlAttribute.contains("fa-chart-bar"))
    {result = FontAwesomeIcons.chartBar;}

    if(htmlAttribute.contains("fa-twitter-square"))
    {result = FontAwesomeIcons.twitterSquare;}

    if(htmlAttribute.contains("fa-facebook-square"))
    {result = FontAwesomeIcons.facebookSquare;}

    if(htmlAttribute.contains("fa-camera-retro"))
    {result = FontAwesomeIcons.cameraRetro;}

    if(htmlAttribute.contains("fa-key"))
    {result = FontAwesomeIcons.key;}

    if(htmlAttribute.contains("fa-cogs"))
    {result = FontAwesomeIcons.cogs;}

    if(htmlAttribute.contains("fa-comments"))
    {result = FontAwesomeIcons.comments;}

    if(htmlAttribute.contains("fa-thumbs-o-up"))
    {result = FontAwesomeIcons.thumbsUp;}

    if(htmlAttribute.contains("fa-thumbs-o-down"))
    {result = FontAwesomeIcons.thumbsDown;}

    if(htmlAttribute.contains("fa-star-half"))
    {result = FontAwesomeIcons.starHalf;}

    if(htmlAttribute.contains("fa-heart"))
    {result = FontAwesomeIcons.heart;}

    if(htmlAttribute.contains("fa-sign-out-alt"))
    {result = FontAwesomeIcons.signOutAlt;}

    if(htmlAttribute.contains("fa-linkedin-square"))
    {result = FontAwesomeIcons.linkedin;}

    if(htmlAttribute.contains("fa-thumb-tack"))
    {result = FontAwesomeIcons.thumbtack;}

    if(htmlAttribute.contains("fa-external-link-alt"))
    {result = FontAwesomeIcons.externalLinkAlt;}

    if(htmlAttribute.contains("fa-sign-in"))
    {result = FontAwesomeIcons.sign;}

    if(htmlAttribute.contains("fa-trophy"))
    {result = FontAwesomeIcons.trophy;}

    if(htmlAttribute.contains("fa-github-square"))
    {result = FontAwesomeIcons.github;}

    if(htmlAttribute.contains("fa-upload"))
    {result = FontAwesomeIcons.upload;}

    if(htmlAttribute.contains("fa-lemon-o"))
    {result = FontAwesomeIcons.lemon;}

    if(htmlAttribute.contains("fa-phone"))
    {result = FontAwesomeIcons.phone;}

    if(htmlAttribute.contains("fa-square-o"))
    {result = FontAwesomeIcons.square;}

    if(htmlAttribute.contains("fa-bookmark-o"))
    {result = FontAwesomeIcons.bookmark;}

    if(htmlAttribute.contains("fa-phone-square"))
    {result = FontAwesomeIcons.phone;}

    if(htmlAttribute.contains("fa-twitter"))
    {result = FontAwesomeIcons.twitter;}

    if(htmlAttribute.contains("fa-facebook"))
    {result = FontAwesomeIcons.facebook;}

    if(htmlAttribute.contains("fa-github"))
    {result = FontAwesomeIcons.github;}

    if(htmlAttribute.contains("fa-unlock"))
    {result = FontAwesomeIcons.unlock;}

    if(htmlAttribute.contains("fa-credit-card"))
    {result = FontAwesomeIcons.creditCard;}

    if(htmlAttribute.contains("fa-rss"))
    {result = FontAwesomeIcons.rss;}

    if(htmlAttribute.contains("fa-hdd-o"))
    {result = FontAwesomeIcons.hdd;}

    if(htmlAttribute.contains("fa-bullhorn"))
    {result = FontAwesomeIcons.bullhorn;}

    if(htmlAttribute.contains("fa-bell"))
    {result = FontAwesomeIcons.bell;}

    if(htmlAttribute.contains("fa-certificate"))
    {result = FontAwesomeIcons.certificate;}

    if(htmlAttribute.contains("fa-hand-point-right"))
    {result = FontAwesomeIcons.handPointRight;}

    if(htmlAttribute.contains("fa-hand-point-left"))
    {result = FontAwesomeIcons.handPointLeft;}

    if(htmlAttribute.contains("fa-hand-point-up"))
    {result = FontAwesomeIcons.handPointUp;}

    if(htmlAttribute.contains("fa-hand-point-down"))
    {result = FontAwesomeIcons.handPointDown;}

    if(htmlAttribute.contains("fa-arrow-circle-left"))
    {result = FontAwesomeIcons.arrowCircleLeft;}

    if(htmlAttribute.contains("fa-arrow-circle-right"))
    {result = FontAwesomeIcons.arrowCircleRight;}

    if(htmlAttribute.contains("fa-arrow-circle-up"))
    {result = FontAwesomeIcons.arrowCircleUp;}

    if(htmlAttribute.contains("fa-arrow-circle-down"))
    {result = FontAwesomeIcons.arrowCircleDown;}

    if(htmlAttribute.contains("fa-globe"))
    {result = FontAwesomeIcons.globe;}

    if(htmlAttribute.contains("fa-wrench"))
    {result = FontAwesomeIcons.wrench;}

    if(htmlAttribute.contains("fa-tasks"))
    {result = FontAwesomeIcons.tasks;}

    if(htmlAttribute.contains("fa-filter"))
    {result = FontAwesomeIcons.filter;}

    if(htmlAttribute.contains("fa-briefcase"))
    {result = FontAwesomeIcons.briefcase;}

    if(htmlAttribute.contains("fa-expand-arrows-alt"))
    {result = FontAwesomeIcons.expandArrowsAlt;}

    if(htmlAttribute.contains("fa-users"))
    {result = FontAwesomeIcons.users;}

    if(htmlAttribute.contains("fa-link"))
    {result = FontAwesomeIcons.link;}

    if(htmlAttribute.contains("fa-cloud"))
    {result = FontAwesomeIcons.cloud;}

    if(htmlAttribute.contains("fa-flask"))
    {result = FontAwesomeIcons.flask;}



    if(htmlAttribute.contains("fa-copy"))
    {result = FontAwesomeIcons.copy;}

    if(htmlAttribute.contains("fa-paperclip"))
    {result = FontAwesomeIcons.paperclip;}

    if(htmlAttribute.contains("fa-save"))
    {result = FontAwesomeIcons.save;}

    if(htmlAttribute.contains("fa-square"))
    {result = FontAwesomeIcons.square;}

    if(htmlAttribute.contains("fa-bars"))
    {result = FontAwesomeIcons.bars;}

    if(htmlAttribute.contains("fa-list-ul"))
    {result = FontAwesomeIcons.list;}

    if(htmlAttribute.contains("fa-list-ol"))
    {result = FontAwesomeIcons.list;}

    if(htmlAttribute.contains("fa-strikethrough"))
    {result = FontAwesomeIcons.strikethrough;}

    if(htmlAttribute.contains("fa-underline"))
    {result = FontAwesomeIcons.underline;}

    if(htmlAttribute.contains("fa-table"))
    {result = FontAwesomeIcons.table;}

    if(htmlAttribute.contains("fa-magic"))
    {result = FontAwesomeIcons.magic;}

    if(htmlAttribute.contains("fa-truck"))
    {result = FontAwesomeIcons.truck;}

    if(htmlAttribute.contains("fa-pinterest"))
    {result = FontAwesomeIcons.pinterest;}

    if(htmlAttribute.contains("fa-pinterest-square"))
    {result = FontAwesomeIcons.pinterest;}

    if(htmlAttribute.contains("fa-google-plus-square"))
    {result = FontAwesomeIcons.google;}

    if(htmlAttribute.contains("fa-google-plus"))
    {result = FontAwesomeIcons.google;}

    if(htmlAttribute.contains("fa-money-bill"))
    {result = FontAwesomeIcons.moneyBill;}

    if(htmlAttribute.contains("fa-caret-down"))
    {result = FontAwesomeIcons.caretDown;}

    if(htmlAttribute.contains("fa-caret-up"))
    {result = FontAwesomeIcons.caretUp;}

    if(htmlAttribute.contains("fa-caret-left"))
    {result = FontAwesomeIcons.caretLeft;}

    if(htmlAttribute.contains("fa-caret-right"))
    {result = FontAwesomeIcons.caretRight;}

    if(htmlAttribute.contains("fa-columns"))
    {result = FontAwesomeIcons.columns;}

    if(htmlAttribute.contains("fa-sort"))
    {result = FontAwesomeIcons.sort;}

    if(htmlAttribute.contains("fa-sort-desc"))
    {result = FontAwesomeIcons.sort;}

    if(htmlAttribute.contains("fa-sort-asc"))
    {result = FontAwesomeIcons.sort;}

    if(htmlAttribute.contains("fa-envelope"))
    {result = FontAwesomeIcons.envelope;}

    if(htmlAttribute.contains("fa-linkedin"))
    {result = FontAwesomeIcons.linkedin;}

    if(htmlAttribute.contains("fa-undo"))
    {result = FontAwesomeIcons.undo;}

    if(htmlAttribute.contains("fa-gavel"))
    {result = FontAwesomeIcons.gavel;}

    if(htmlAttribute.contains("fa-tachometer"))
    {result = FontAwesomeIcons.tachometerAlt;}

    if(htmlAttribute.contains("fa-comment-o"))
    {result = FontAwesomeIcons.comment;}

    if(htmlAttribute.contains("fa-comments-o"))
    {result = FontAwesomeIcons.comments;}

    if(htmlAttribute.contains("fa-bolt"))
    {result = FontAwesomeIcons.bolt;}

    if(htmlAttribute.contains("fa-sitemap"))
    {result = FontAwesomeIcons.sitemap;}

    if(htmlAttribute.contains("fa-umbrella"))
    {result = FontAwesomeIcons.umbrella;}

    if(htmlAttribute.contains("fa-clipboard"))
    {result = FontAwesomeIcons.clipboard;}

    if(htmlAttribute.contains("fa-lightbulb-o"))
    {result = FontAwesomeIcons.lightbulb;}

    if(htmlAttribute.contains("fa-exchange"))
    {result = FontAwesomeIcons.exchangeAlt;}

    if(htmlAttribute.contains("fa-cloud-download"))
    {result = FontAwesomeIcons.cloud;}

    if(htmlAttribute.contains("fa-cloud-upload"))
    {result = FontAwesomeIcons.cloud;}

    if(htmlAttribute.contains("fa-user-md"))
    {result = FontAwesomeIcons.user;}

    if(htmlAttribute.contains("fa-stethoscope"))
    {result = FontAwesomeIcons.stethoscope;}

    if(htmlAttribute.contains("fa-suitcase"))
    {result = FontAwesomeIcons.suitcase;}

    if(htmlAttribute.contains("fa-bell-o"))
    {result = FontAwesomeIcons.bell;}

    if(htmlAttribute.contains("fa-coffee"))
    {result = FontAwesomeIcons.coffee;}

    if(htmlAttribute.contains("fa-utensils"))
    {result = FontAwesomeIcons.utensils;}

    if(htmlAttribute.contains("fa-file-text-o"))
    {result = FontAwesomeIcons.file;}

    if(htmlAttribute.contains("fa-building-o"))
    {result = FontAwesomeIcons.building;}

    if(htmlAttribute.contains("fa-hospital-o"))
    {result = FontAwesomeIcons.hospital;}

    if(htmlAttribute.contains("fa-ambulance"))
    {result = FontAwesomeIcons.ambulance;}

    if(htmlAttribute.contains("fa-medkit"))
    {result = FontAwesomeIcons.medkit;}

    if(htmlAttribute.contains("fa-fighter-jet"))
    {result = FontAwesomeIcons.fighterJet;}

    if(htmlAttribute.contains("fa-beer"))
    {result = FontAwesomeIcons.beer;}

    if(htmlAttribute.contains("fa-h-square"))
    {result = FontAwesomeIcons.hSquare;}

    if(htmlAttribute.contains("fa-plus-square"))
    {result = FontAwesomeIcons.plus;}

    if(htmlAttribute.contains("fa-angle-double-left"))
    {result = FontAwesomeIcons.angleDoubleLeft;}

    if(htmlAttribute.contains("fa-angle-double-right"))
    {result = FontAwesomeIcons.angleDoubleRight;}

    if(htmlAttribute.contains("fa-angle-double-up"))
    {result = FontAwesomeIcons.angleDoubleUp;}

    if(htmlAttribute.contains("fa-angle-double-down"))
    {result = FontAwesomeIcons.angleDoubleDown;}

    if(htmlAttribute.contains("fa-angle-left"))
    {result = FontAwesomeIcons.angleLeft;}

    if(htmlAttribute.contains("fa-angle-right"))
    {result = FontAwesomeIcons.angleRight;}

    if(htmlAttribute.contains("fa-angle-up"))
    {result = FontAwesomeIcons.angleUp;}

    if(htmlAttribute.contains("fa-angle-down"))
    {result = FontAwesomeIcons.angleDown;}

    if(htmlAttribute.contains("fa-desktop"))
    {result = FontAwesomeIcons.desktop;}

    if(htmlAttribute.contains("fa-laptop"))
    {result = FontAwesomeIcons.laptop;}

    if(htmlAttribute.contains("fa-tablet"))
    {result = FontAwesomeIcons.tablet;}

    if(htmlAttribute.contains("fa-mobile"))
    {result = FontAwesomeIcons.mobile;}

    if(htmlAttribute.contains("fa-circle-o"))
    {result = FontAwesomeIcons.circle;}

    if(htmlAttribute.contains("fa-quote-left"))
    {result = FontAwesomeIcons.quoteLeft;}

    if(htmlAttribute.contains("fa-quote-right"))
    {result = FontAwesomeIcons.quoteRight;}

    if(htmlAttribute.contains("fa-spinner"))
    {result = FontAwesomeIcons.spinner;}

    if(htmlAttribute.contains("fa-circle"))
    {result = FontAwesomeIcons.circle;}

    if(htmlAttribute.contains("fa-reply"))
    {result = FontAwesomeIcons.reply;}

    if(htmlAttribute.contains("fa-github-alt"))
    {result = FontAwesomeIcons.github;}

    if(htmlAttribute.contains("fa-folder-o"))
    {result = FontAwesomeIcons.folder;}

    if(htmlAttribute.contains("fa-folder-open-o"))
    {result = FontAwesomeIcons.folderOpen;}

    if(htmlAttribute.contains("fa-smile-o"))
    {result = FontAwesomeIcons.smile;}

    if(htmlAttribute.contains("fa-frown-o"))
    {result = FontAwesomeIcons.frown;}

    if(htmlAttribute.contains("fa-meh-o"))
    {result = FontAwesomeIcons.meh;}

    if(htmlAttribute.contains("fa-gamepad"))
    {result = FontAwesomeIcons.gamepad;}

    if(htmlAttribute.contains("fa-keyboard-o"))
    {result = FontAwesomeIcons.keyboard;}

    if(htmlAttribute.contains("fa-flag-o"))
    {result = FontAwesomeIcons.flag;}

    if(htmlAttribute.contains("fa-flag-checkered"))
    {result = FontAwesomeIcons.flag;}

    if(htmlAttribute.contains("fa-terminal"))
    {result = FontAwesomeIcons.terminal;}

    if(htmlAttribute.contains("fa-code"))
    {result = FontAwesomeIcons.code;}

    if(htmlAttribute.contains("fa-reply-all"))
    {result = FontAwesomeIcons.reply;}

    if(htmlAttribute.contains("fa-star-half-o"))
    {result = FontAwesomeIcons.star;}

    if(htmlAttribute.contains("fa-location-arrow"))
    {result = FontAwesomeIcons.locationArrow;}

    if(htmlAttribute.contains("fa-crop"))
    {result = FontAwesomeIcons.crop;}

    if(htmlAttribute.contains("fa-code-fork"))
    {result = FontAwesomeIcons.code;}

    if(htmlAttribute.contains("fa-unlink"))
    {result = FontAwesomeIcons.unlink;}

    if(htmlAttribute.contains("fa-question"))
    {result = FontAwesomeIcons.question;}

    if(htmlAttribute.contains("fa-info"))
    {result = FontAwesomeIcons.info;}

    if(htmlAttribute.contains("fa-exclamation"))
    {result = FontAwesomeIcons.exclamation;}

    if(htmlAttribute.contains("fa-superscript"))
    {result = FontAwesomeIcons.superscript;}

    if(htmlAttribute.contains("fa-subscript"))
    {result = FontAwesomeIcons.subscript;}

    if(htmlAttribute.contains("fa-eraser"))
    {result = FontAwesomeIcons.eraser;}

    if(htmlAttribute.contains("fa-puzzle-piece"))
    {result = FontAwesomeIcons.puzzlePiece;}

    if(htmlAttribute.contains("fa-microphone"))
    {result = FontAwesomeIcons.microphone;}

    if(htmlAttribute.contains("fa-microphone-slash"))
    {result = FontAwesomeIcons.microphone;}

    if(htmlAttribute.contains("fa-shield"))
    {result = FontAwesomeIcons.shieldAlt;}

    if(htmlAttribute.contains("fa-calendar-o"))
    {result = FontAwesomeIcons.calendar;}

    if(htmlAttribute.contains("fa-fire-extinguisher"))
    {result = FontAwesomeIcons.fire;}

    if(htmlAttribute.contains("fa-rocket"))
    {result = FontAwesomeIcons.rocket;}

    if(htmlAttribute.contains("fa-maxcdn"))
    {result = FontAwesomeIcons.maxcdn;}

    if(htmlAttribute.contains("fa-chevron-circle-left"))
    {result = FontAwesomeIcons.chevronCircleLeft;}

    if(htmlAttribute.contains("fa-chevron-circle-right"))
    {result = FontAwesomeIcons.chevronCircleRight;}

    if(htmlAttribute.contains("fa-chevron-circle-up"))
    {result = FontAwesomeIcons.chevronCircleUp;}

    if(htmlAttribute.contains("fa-chevron-circle-down"))
    {result = FontAwesomeIcons.chevronCircleDown;}

    if(htmlAttribute.contains("fa-html5"))
    {result = FontAwesomeIcons.html5;}

    if(htmlAttribute.contains("fa-css3"))
    {result = FontAwesomeIcons.css3;}

    if(htmlAttribute.contains("fa-anchor"))
    {result = FontAwesomeIcons.anchor;}

    if(htmlAttribute.contains("fa-unlock-alt"))
    {result = FontAwesomeIcons.unlock;}

    if(htmlAttribute.contains("fa-bullseye"))
    {result = FontAwesomeIcons.bullseye;}

    if(htmlAttribute.contains("fa-ellipsis-h"))
    {result = FontAwesomeIcons.ellipsisH;}

    if(htmlAttribute.contains("fa-ellipsis-v"))
    {result = FontAwesomeIcons.ellipsisV;}

    if(htmlAttribute.contains("fa-rss-square"))
    {result = FontAwesomeIcons.rss;}

    if(htmlAttribute.contains("fa-play-circle"))
    {result = FontAwesomeIcons.play;}

    if(htmlAttribute.contains("fa-ticket"))
    {result = FontAwesomeIcons.ticketAlt;}

    if(htmlAttribute.contains("fa-minus-square"))
    {result = FontAwesomeIcons.minus;}

    if(htmlAttribute.contains("fa-minus-square-o"))
    {result = FontAwesomeIcons.minus;}

    if(htmlAttribute.contains("fa-level-up"))
    {result = FontAwesomeIcons.levelUpAlt;}

    if(htmlAttribute.contains("fa-level-down"))
    {result = FontAwesomeIcons.levelDownAlt;}

    if(htmlAttribute.contains("fa-check-square"))
    {result = FontAwesomeIcons.check;}

    if(htmlAttribute.contains("fa-pen-square"))
    {result = FontAwesomeIcons.penSquare;}

    if(htmlAttribute.contains("fa-external-link-square"))
    {result = FontAwesomeIcons.externalLinkSquareAlt;}

    if(htmlAttribute.contains("fa-share-square"))
    {result = FontAwesomeIcons.share;}

    if(htmlAttribute.contains("fa-compass"))
    {result = FontAwesomeIcons.compass;}

    if(htmlAttribute.contains("fa-caret-square-o-down"))
    {result = FontAwesomeIcons.caretSquareDown;}

    if(htmlAttribute.contains("fa-caret-square-o-up"))
    {result = FontAwesomeIcons.caretSquareUp;}

    if(htmlAttribute.contains("fa-caret-square-o-right"))
    {result = FontAwesomeIcons.caretSquareRight;}

    if(htmlAttribute.contains("fa-euro-sign"))
    {result = FontAwesomeIcons.euroSign;}

    if(htmlAttribute.contains("fa-pound-sign"))
    {result = FontAwesomeIcons.poundSign;}

    if(htmlAttribute.contains("fa-dollar-sign"))
    {result = FontAwesomeIcons.dollarSign;}

    if(htmlAttribute.contains("fa-rupee-sign"))
    {result = FontAwesomeIcons.rupeeSign;}

    if(htmlAttribute.contains("fa-yen-sign"))
    {result = FontAwesomeIcons.yenSign;}

    if(htmlAttribute.contains("fa-ruble-sign"))
    {result = FontAwesomeIcons.rubleSign;}

    if(htmlAttribute.contains("fa-won-sign"))
    {result = FontAwesomeIcons.wonSign;}

    if(htmlAttribute.contains("fa-btc"))
    {result = FontAwesomeIcons.btc;}

    if(htmlAttribute.contains("fa-file"))
    {result = FontAwesomeIcons.file;}

    if(htmlAttribute.contains("fa-file"))
    {result = FontAwesomeIcons.fileAlt;}

    if(htmlAttribute.contains("fa-sort-alpha-up"))
    {result = FontAwesomeIcons.sortAlphaUp;}

    if(htmlAttribute.contains("fa-sort-alpha-down"))
    {result = FontAwesomeIcons.sortAlphaDown;}

    if(htmlAttribute.contains("fa-sort-amount-up"))
    {result = FontAwesomeIcons.sortAmountUp;}

    if(htmlAttribute.contains("fa-sort-amount-down"))
    {result = FontAwesomeIcons.sortAmountDown;}

    if(htmlAttribute.contains("fa-sort-numeric-up"))
    {result = FontAwesomeIcons.sortNumericUp;}

    if(htmlAttribute.contains("fa-sort-numeric-down"))
    {result = FontAwesomeIcons.sortNumericDown;}

    if(htmlAttribute.contains("fa-thumbs-up"))
    {result = FontAwesomeIcons.thumbsUp;}

    if(htmlAttribute.contains("fa-thumbs-down"))
    {result = FontAwesomeIcons.thumbsDown;}

    if(htmlAttribute.contains("fa-youtube-square"))
    {result = FontAwesomeIcons.youtubeSquare;}

    if(htmlAttribute.contains("fa-youtube"))
    {result = FontAwesomeIcons.youtube;}

    if(htmlAttribute.contains("fa-xing"))
    {result = FontAwesomeIcons.xing;}

    if(htmlAttribute.contains("fa-xing-square"))
    {result = FontAwesomeIcons.xingSquare;}

    if(htmlAttribute.contains("fa-dropbox"))
    {result = FontAwesomeIcons.dropbox;}

    if(htmlAttribute.contains("fa-stack-overflow"))
    {result = FontAwesomeIcons.stackOverflow;}

    if(htmlAttribute.contains("fa-instagram"))
    {result = FontAwesomeIcons.instagram;}

    if(htmlAttribute.contains("fa-flickr"))
    {result = FontAwesomeIcons.flickr;}

    if(htmlAttribute.contains("fa-adn"))
    {result = FontAwesomeIcons.adn;}

    if(htmlAttribute.contains("fa-bitbucket"))
    {result = FontAwesomeIcons.bitbucket;}

    if(htmlAttribute.contains("fa-tumblr"))
    {result = FontAwesomeIcons.tumblr;}

    if(htmlAttribute.contains("fa-tumblr-square"))
    {result = FontAwesomeIcons.tumblrSquare;}

    if(htmlAttribute.contains("fa-long-arrow-alt-down"))
    {result = FontAwesomeIcons.longArrowAltDown;}

    if(htmlAttribute.contains("fa-long-arrow-alt-up"))
    {result = FontAwesomeIcons.longArrowAltUp;}

    if(htmlAttribute.contains("fa-long-arrow-alt-left"))
    {result = FontAwesomeIcons.longArrowAltLeft;}

    if(htmlAttribute.contains("fa-long-arrow-alt-right"))
    {result = FontAwesomeIcons.longArrowAltRight;}

    if(htmlAttribute.contains("fa-apple"))
    {result = FontAwesomeIcons.apple;}

    if(htmlAttribute.contains("fa-windows"))
    {result = FontAwesomeIcons.windows;}

    if(htmlAttribute.contains("fa-android"))
    {result = FontAwesomeIcons.android;}

    if(htmlAttribute.contains("fa-linux"))
    {result = FontAwesomeIcons.linux;}

    if(htmlAttribute.contains("fa-dribbble"))
    {result = FontAwesomeIcons.dribbble;}

    if(htmlAttribute.contains("fa-skype"))
    {result = FontAwesomeIcons.skype;}

    if(htmlAttribute.contains("fa-foursquare"))
    {result = FontAwesomeIcons.foursquare;}

    if(htmlAttribute.contains("fa-trello"))
    {result = FontAwesomeIcons.trello;}

    if(htmlAttribute.contains("fa-female"))
    {result = FontAwesomeIcons.female;}

    if(htmlAttribute.contains("fa-male"))
    {result = FontAwesomeIcons.male;}

    if(htmlAttribute.contains("fa-gratipay"))
    {result = FontAwesomeIcons.gratipay;}

    if(htmlAttribute.contains("fa-sun-o"))
    {result = FontAwesomeIcons.sun;}

    if(htmlAttribute.contains("fa-moon-o"))
    {result = FontAwesomeIcons.moon;}

    if(htmlAttribute.contains("fa-archive"))
    {result = FontAwesomeIcons.archive;}

    if(htmlAttribute.contains("fa-bug"))
    {result = FontAwesomeIcons.bug;}

    if(htmlAttribute.contains("fa-vk"))
    {result = FontAwesomeIcons.vk;}

    if(htmlAttribute.contains("fa-weibo"))
    {result = FontAwesomeIcons.weibo;}

    if(htmlAttribute.contains("fa-renren"))
    {result = FontAwesomeIcons.renren;}

    if(htmlAttribute.contains("fa-pagelines"))
    {result = FontAwesomeIcons.pagelines;}

    if(htmlAttribute.contains("fa-stack-exchange"))
    {result = FontAwesomeIcons.stackExchange;}

    if(htmlAttribute.contains("fa-arrow-circle-right"))
    {result = FontAwesomeIcons.arrowCircleRight;}

    if(htmlAttribute.contains("fa-arrow-circle-left"))
    {result = FontAwesomeIcons.arrowCircleLeft;}

    if(htmlAttribute.contains("fa-caret-square-left"))
    {result = FontAwesomeIcons.caretSquareLeft;}

    if(htmlAttribute.contains("fa-dot-circle"))
    {result = FontAwesomeIcons.dotCircle;}

    if(htmlAttribute.contains("fa-wheelchair"))
    {result = FontAwesomeIcons.wheelchair;}

    if(htmlAttribute.contains("fa-vimeo-square"))
    {result = FontAwesomeIcons.vimeo;}

    if(htmlAttribute.contains("fa-lira-sign"))
    {result = FontAwesomeIcons.liraSign;}

    if(htmlAttribute.contains("fa-plus-square"))
    {result = FontAwesomeIcons.plusSquare;}

    if(htmlAttribute.contains("fa-space-shuttle"))
    {result = FontAwesomeIcons.spaceShuttle;}

    if(htmlAttribute.contains("fa-slack"))
    {result = FontAwesomeIcons.slack;}

    if(htmlAttribute.contains("fa-envelope-square"))
    {result = FontAwesomeIcons.envelopeSquare;}

    if(htmlAttribute.contains("fa-wordpress"))
    {result = FontAwesomeIcons.wordpress;}

    if(htmlAttribute.contains("fa-openid"))
    {result = FontAwesomeIcons.openid;}

    if(htmlAttribute.contains("fa-university"))
    {result = FontAwesomeIcons.university;}

    if(htmlAttribute.contains("fa-graduation-cap"))
    {result = FontAwesomeIcons.graduationCap;}

    if(htmlAttribute.contains("fa-yahoo"))
    {result = FontAwesomeIcons.yahoo;}

    if(htmlAttribute.contains("fa-google"))
    {result = FontAwesomeIcons.google;}

    if(htmlAttribute.contains("fa-reddit"))
    {result = FontAwesomeIcons.reddit;}

    if(htmlAttribute.contains("fa-reddit-square"))
    {result = FontAwesomeIcons.redditSquare;}

    if(htmlAttribute.contains("fa-stumbleupon-circle"))
    {result = FontAwesomeIcons.stumbleuponCircle;}

    if(htmlAttribute.contains("fa-stumbleupon"))
    {result = FontAwesomeIcons.stumbleupon;}

    if(htmlAttribute.contains("fa-delicious"))
    {result = FontAwesomeIcons.delicious;}

    if(htmlAttribute.contains("fa-digg"))
    {result = FontAwesomeIcons.digg;}

    if(htmlAttribute.contains("fa-pied-piper"))
    {result = FontAwesomeIcons.piedPiper;}

    if(htmlAttribute.contains("fa-pied-piper-alt"))
    {result = FontAwesomeIcons.piedPiperAlt;}

    if(htmlAttribute.contains("fa-drupal"))
    {result = FontAwesomeIcons.drupal;}

    if(htmlAttribute.contains("fa-joomla"))
    {result = FontAwesomeIcons.joomla;}

    if(htmlAttribute.contains("fa-language"))
    {result = FontAwesomeIcons.language;}

    if(htmlAttribute.contains("fa-fax"))
    {result = FontAwesomeIcons.fax;}

    if(htmlAttribute.contains("fa-building"))
    {result = FontAwesomeIcons.building;}

    if(htmlAttribute.contains("fa-child"))
    {result = FontAwesomeIcons.child;}

    if(htmlAttribute.contains("fa-paw"))
    {result = FontAwesomeIcons.paw;}

    if(htmlAttribute.contains("fa-utensil-spoon"))
    {result = FontAwesomeIcons.utensilSpoon;}

    if(htmlAttribute.contains("fa-cube"))
    {result = FontAwesomeIcons.cube;}

    if(htmlAttribute.contains("fa-cubes"))
    {result = FontAwesomeIcons.cubes;}

    if(htmlAttribute.contains("fa-behance"))
    {result = FontAwesomeIcons.behance;}

    if(htmlAttribute.contains("fa-behance-square"))
    {result = FontAwesomeIcons.behanceSquare;}

    if(htmlAttribute.contains("fa-steam"))
    {result = FontAwesomeIcons.steam;}

    if(htmlAttribute.contains("fa-steam-square"))
    {result = FontAwesomeIcons.steamSquare;}

    if(htmlAttribute.contains("fa-recycle"))
    {result = FontAwesomeIcons.recycle;}

    if(htmlAttribute.contains("fa-car"))
    {result = FontAwesomeIcons.car;}

    if(htmlAttribute.contains("fa-taxi"))
    {result = FontAwesomeIcons.taxi;}

    if(htmlAttribute.contains("fa-tree"))
    {result = FontAwesomeIcons.tree;}

    if(htmlAttribute.contains("fa-spotify"))
    {result = FontAwesomeIcons.spotify;}

    if(htmlAttribute.contains("fa-deviantart"))
    {result = FontAwesomeIcons.deviantart;}

    if(htmlAttribute.contains("fa-soundcloud"))
    {result = FontAwesomeIcons.soundcloud;}

    if(htmlAttribute.contains("fa-database"))
    {result = FontAwesomeIcons.database;}

    if(htmlAttribute.contains("fa-file-pdf"))
    {result = FontAwesomeIcons.filePdf;}

    if(htmlAttribute.contains("fa-file-word"))
    {result = FontAwesomeIcons.fileWord;}

    if(htmlAttribute.contains("fa-file-excel"))
    {result = FontAwesomeIcons.fileExcel;}

    if(htmlAttribute.contains("fa-file-powerpoint"))
    {result = FontAwesomeIcons.filePowerpoint;}

    if(htmlAttribute.contains("fa-file-image"))
    {result = FontAwesomeIcons.fileImage;}

    if(htmlAttribute.contains("fa-file-archive"))
    {result = FontAwesomeIcons.fileArchive;}

    if(htmlAttribute.contains("fa-file-audio"))
    {result = FontAwesomeIcons.fileAudio;}

    if(htmlAttribute.contains("fa-file-video"))
    {result = FontAwesomeIcons.fileVideo;}

    if(htmlAttribute.contains("fa-file-code"))
    {result = FontAwesomeIcons.fileCode;}

    if(htmlAttribute.contains("fa-vine"))
    {result = FontAwesomeIcons.vine;}

    if(htmlAttribute.contains("fa-codepen"))
    {result = FontAwesomeIcons.codepen;}

    if(htmlAttribute.contains("fa-jsfiddle"))
    {result = FontAwesomeIcons.jsfiddle;}

    if(htmlAttribute.contains("fa-life-ring"))
    {result = FontAwesomeIcons.lifeRing;}

    if(htmlAttribute.contains("fa-circle-notch"))
    {result = FontAwesomeIcons.circleNotch;}

    if(htmlAttribute.contains("fa-rebel"))
    {result = FontAwesomeIcons.rebel;}

    if(htmlAttribute.contains("fa-empire"))
    {result = FontAwesomeIcons.empire;}

    if(htmlAttribute.contains("fa-git-square"))
    {result = FontAwesomeIcons.gitSquare;}

    if(htmlAttribute.contains("fa-git"))
    {result = FontAwesomeIcons.git;}

    if(htmlAttribute.contains("fa-hacker-news"))
    {result = FontAwesomeIcons.hackerNews;}

    if(htmlAttribute.contains("fa-tencent-weibo"))
    {result = FontAwesomeIcons.tencentWeibo;}

    if(htmlAttribute.contains("fa-qq"))
    {result = FontAwesomeIcons.qq;}

    if(htmlAttribute.contains("fa-weixin"))
    {result = FontAwesomeIcons.weixin;}

    if(htmlAttribute.contains("fa-paper-plane"))
    {result = FontAwesomeIcons.paperPlane;}

    if(htmlAttribute.contains("fa-history"))
    {result = FontAwesomeIcons.history;}

    if(htmlAttribute.contains("fa-circle-thin"))
    {result = FontAwesomeIcons.circle;}

    if(htmlAttribute.contains("fa-heading"))
    {result = FontAwesomeIcons.heading;}

    if(htmlAttribute.contains("fa-paragraph"))
    {result = FontAwesomeIcons.paragraph;}

    if(htmlAttribute.contains("fa-sliders-h"))
    {result = FontAwesomeIcons.slidersH;}

    if(htmlAttribute.contains("fa-share-alt"))
    {result = FontAwesomeIcons.shareAlt;}

    if(htmlAttribute.contains("fa-share-alt-square"))
    {result = FontAwesomeIcons.shareAltSquare;}

    if(htmlAttribute.contains("fa-bomb"))
    {result = FontAwesomeIcons.bomb;}

    if(htmlAttribute.contains("fa-futbol"))
    {result = FontAwesomeIcons.futbol;}

    if(htmlAttribute.contains("fa-tty"))
    {result = FontAwesomeIcons.tty;}

    if(htmlAttribute.contains("fa-binoculars"))
    {result = FontAwesomeIcons.binoculars;}

    if(htmlAttribute.contains("fa-plug"))
    {result = FontAwesomeIcons.plug;}

    if(htmlAttribute.contains("fa-slideshare"))
    {result = FontAwesomeIcons.slideshare;}

    if(htmlAttribute.contains("fa-twitch"))
    {result = FontAwesomeIcons.twitch;}

    if(htmlAttribute.contains("fa-yelp"))
    {result = FontAwesomeIcons.yelp;}

    if(htmlAttribute.contains("fa-newspaper"))
    {result = FontAwesomeIcons.newspaper;}

    if(htmlAttribute.contains("fa-wifi"))
    {result = FontAwesomeIcons.wifi;}

    if(htmlAttribute.contains("fa-calculator"))
    {result = FontAwesomeIcons.calculator;}

    if(htmlAttribute.contains("fa-paypal"))
    {result = FontAwesomeIcons.paypal;}

    if(htmlAttribute.contains("fa-google-wallet"))
    {result = FontAwesomeIcons.google;}

    if(htmlAttribute.contains("fa-cc-visa"))
    {result = FontAwesomeIcons.ccVisa;}

    if(htmlAttribute.contains("fa-cc-mastercard"))
    {result = FontAwesomeIcons.ccMastercard;}

    if(htmlAttribute.contains("fa-cc-discover"))
    {result = FontAwesomeIcons.ccDiscover;}

    if(htmlAttribute.contains("fa-cc-amex"))
    {result = FontAwesomeIcons.ccAmex;}

    if(htmlAttribute.contains("fa-cc-paypal"))
    {result = FontAwesomeIcons.ccPaypal;}

    if(htmlAttribute.contains("fa-cc-stripe"))
    {result = FontAwesomeIcons.ccStripe;}

    if(htmlAttribute.contains("fa-bell-slash"))
    {result = FontAwesomeIcons.bellSlash;}

    if(htmlAttribute.contains("fa-trash"))
    {result = FontAwesomeIcons.trash;}

    if(htmlAttribute.contains("fa-copyright"))
    {result = FontAwesomeIcons.copyright;}

    if(htmlAttribute.contains("fa-at"))
    {result = FontAwesomeIcons.at;}

    if(htmlAttribute.contains("fa-eye-dropper"))
    {result = FontAwesomeIcons.eyeDropper;}

    if(htmlAttribute.contains("fa-paint-brush"))
    {result = FontAwesomeIcons.paintBrush;}

    if(htmlAttribute.contains("fa-birthday-cake"))
    {result = FontAwesomeIcons.birthdayCake;}

    if(htmlAttribute.contains("fa-chart-area"))
    {result = FontAwesomeIcons.chartArea;}

    if(htmlAttribute.contains("fa-chart-pie"))
    {result = FontAwesomeIcons.chartPie;}

    if(htmlAttribute.contains("fa-line-chart"))
    {result = FontAwesomeIcons.line;}

    if(htmlAttribute.contains("fa-lastfm"))
    {result = FontAwesomeIcons.lastfm;}

    if(htmlAttribute.contains("fa-lastfm-square"))
    {result = FontAwesomeIcons.lastfm;}

    if(htmlAttribute.contains("fa-toggle-off"))
    {result = FontAwesomeIcons.toggleOff;}

    if(htmlAttribute.contains("fa-toggle-on"))
    {result = FontAwesomeIcons.toggleOn;}

    if(htmlAttribute.contains("fa-bicycle"))
    {result = FontAwesomeIcons.bicycle;}

    if(htmlAttribute.contains("fa-bus"))
    {result = FontAwesomeIcons.bus;}

    if(htmlAttribute.contains("fa-ioxhost"))
    {result = FontAwesomeIcons.ioxhost;}

    if(htmlAttribute.contains("fa-angellist"))
    {result = FontAwesomeIcons.angellist;}

    if(htmlAttribute.contains("fa-closed-captioning"))
    {result = FontAwesomeIcons.closedCaptioning;}

    if(htmlAttribute.contains("fa-shekel-sign"))
    {result = FontAwesomeIcons.shekelSign;}

    if(htmlAttribute.contains("fa-buysellads"))
    {result = FontAwesomeIcons.buysellads;}

    if(htmlAttribute.contains("fa-connectdevelop"))
    {result = FontAwesomeIcons.connectdevelop;}

    if(htmlAttribute.contains("fa-dashcube"))
    {result = FontAwesomeIcons.dashcube;}

    if(htmlAttribute.contains("fa-forumbee"))
    {result = FontAwesomeIcons.forumbee;}

    if(htmlAttribute.contains("fa-leanpub"))
    {result = FontAwesomeIcons.leanpub;}

    if(htmlAttribute.contains("fa-sellsy"))
    {result = FontAwesomeIcons.sellsy;}

    if(htmlAttribute.contains("fa-shirtsinbulk"))
    {result = FontAwesomeIcons.shirtsinbulk;}

    if(htmlAttribute.contains("fa-simplybuilt"))
    {result = FontAwesomeIcons.simplybuilt;}

    if(htmlAttribute.contains("fa-skyatlas"))
    {result = FontAwesomeIcons.skyatlas;}

    if(htmlAttribute.contains("fa-cart-plus"))
    {result = FontAwesomeIcons.cartPlus;}

    if(htmlAttribute.contains("fa-cart-arrow-down"))
    {result = FontAwesomeIcons.cartArrowDown;}

    if(htmlAttribute.contains("fa-gem"))
    {result = FontAwesomeIcons.gem;}

    if(htmlAttribute.contains("fa-ship"))
    {result = FontAwesomeIcons.ship;}

    if(htmlAttribute.contains("fa-user-secret"))
    {result = FontAwesomeIcons.user;}

    if(htmlAttribute.contains("fa-motorcycle"))
    {result = FontAwesomeIcons.motorcycle;}

    if(htmlAttribute.contains("fa-street-view"))
    {result = FontAwesomeIcons.streetView;}

    if(htmlAttribute.contains("fa-heartbeat"))
    {result = FontAwesomeIcons.heartbeat;}

    if(htmlAttribute.contains("fa-venus"))
    {result = FontAwesomeIcons.venus;}

    if(htmlAttribute.contains("fa-mars"))
    {result = FontAwesomeIcons.mars;}

    if(htmlAttribute.contains("fa-mercury"))
    {result = FontAwesomeIcons.mercury;}

    if(htmlAttribute.contains("fa-transgender"))
    {result = FontAwesomeIcons.transgender;}

    if(htmlAttribute.contains("fa-transgender-alt"))
    {result = FontAwesomeIcons.transgenderAlt;}

    if(htmlAttribute.contains("fa-venus-double"))
    {result = FontAwesomeIcons.venusDouble;}

    if(htmlAttribute.contains("fa-mars-double"))
    {result = FontAwesomeIcons.marsDouble;}

    if(htmlAttribute.contains("fa-venus-mars"))
    {result = FontAwesomeIcons.venusMars;}

    if(htmlAttribute.contains("fa-mars-stroke"))
    {result = FontAwesomeIcons.marsStroke;}

    if(htmlAttribute.contains("fa-mars-stroke-v"))
    {result = FontAwesomeIcons.marsStrokeV;}

    if(htmlAttribute.contains("fa-mars-stroke-h"))
    {result = FontAwesomeIcons.marsStrokeH;}

    if(htmlAttribute.contains("fa-neuter"))
    {result = FontAwesomeIcons.neuter;}

    if(htmlAttribute.contains("fa-genderless"))
    {result = FontAwesomeIcons.genderless;}

    if(htmlAttribute.contains("fa-pinterest-p"))
    {result = FontAwesomeIcons.pinterestP;}

    if(htmlAttribute.contains("fa-whatsapp"))
    {result = FontAwesomeIcons.whatsapp;}

    if(htmlAttribute.contains("fa-server"))
    {result = FontAwesomeIcons.server;}

    if(htmlAttribute.contains("fa-user-plus"))
    {result = FontAwesomeIcons.userPlus;}

    if(htmlAttribute.contains("fa-user-times"))
    {result = FontAwesomeIcons.userTimes;}

    if(htmlAttribute.contains("fa-bed"))
    {result = FontAwesomeIcons.bed;}

    if(htmlAttribute.contains("fa-viacoin"))
    {result = FontAwesomeIcons.viacoin;}

    if(htmlAttribute.contains("fa-train"))
    {result = FontAwesomeIcons.train;}

    if(htmlAttribute.contains("fa-subway"))
    {result = FontAwesomeIcons.subway;}

    if(htmlAttribute.contains("fa-medium"))
    {result = FontAwesomeIcons.medium;}

    if(htmlAttribute.contains("fa-y-combinator"))
    {result = FontAwesomeIcons.yCombinator;}

    if(htmlAttribute.contains("fa-optin-monster"))
    {result = FontAwesomeIcons.optinMonster;}

    if(htmlAttribute.contains("fa-opencart"))
    {result = FontAwesomeIcons.opencart;}

    if(htmlAttribute.contains("fa-expeditedssl"))
    {result = FontAwesomeIcons.expeditedssl;}

    if(htmlAttribute.contains("fa-battery-full"))
    {result = FontAwesomeIcons.batteryFull;}

    if(htmlAttribute.contains("fa-battery-three-quarters"))
    {result = FontAwesomeIcons.batteryThreeQuarters;}

    if(htmlAttribute.contains("fa-battery-half"))
    {result = FontAwesomeIcons.batteryHalf;}

    if(htmlAttribute.contains("fa-battery-quarter"))
    {result = FontAwesomeIcons.batteryQuarter;}

    if(htmlAttribute.contains("fa-battery-empty"))
    {result = FontAwesomeIcons.batteryEmpty;}

    if(htmlAttribute.contains("fa-mouse-pointer"))
    {result = FontAwesomeIcons.mousePointer;}

    if(htmlAttribute.contains("fa-i-cursor"))
    {result = FontAwesomeIcons.iCursor;}

    if(htmlAttribute.contains("fa-object-group"))
    {result = FontAwesomeIcons.objectGroup;}

    if(htmlAttribute.contains("fa-object-ungroup"))
    {result = FontAwesomeIcons.objectUngroup;}

    if(htmlAttribute.contains("fa-sticky-note"))
    {result = FontAwesomeIcons.stickyNote;}

    if(htmlAttribute.contains("fa-cc-jcb"))
    {result = FontAwesomeIcons.ccJcb;}

    if(htmlAttribute.contains("fa-cc-diners-club"))
    {result = FontAwesomeIcons.ccDinersClub;}

    if(htmlAttribute.contains("fa-clone"))
    {result = FontAwesomeIcons.clone;}

    if(htmlAttribute.contains("fa-balance-scale"))
    {result = FontAwesomeIcons.balanceScale;}

    if(htmlAttribute.contains("fa-hourglass-o"))
    {result = FontAwesomeIcons.hourglass;}

    if(htmlAttribute.contains("fa-hourglass-start"))
    {result = FontAwesomeIcons.hourglass;}

    if(htmlAttribute.contains("fa-hourglass-half"))
    {result = FontAwesomeIcons.hourglass;}

    if(htmlAttribute.contains("fa-hourglass-end"))
    {result = FontAwesomeIcons.hourglass;}

    if(htmlAttribute.contains("fa-hourglass"))
    {result = FontAwesomeIcons.hourglass;}

    if(htmlAttribute.contains("fa-hand-rock"))
    {result = FontAwesomeIcons.handRock;}

    if(htmlAttribute.contains("fa-hand-paper"))
    {result = FontAwesomeIcons.handPaper;}

    if(htmlAttribute.contains("fa-hand-scissors"))
    {result = FontAwesomeIcons.handScissors;}

    if(htmlAttribute.contains("fa-hand-lizard"))
    {result = FontAwesomeIcons.handLizard;}

    if(htmlAttribute.contains("fa-hand-spock"))
    {result = FontAwesomeIcons.handSpock;}

    if(htmlAttribute.contains("fa-hand-pointer"))
    {result = FontAwesomeIcons.handPointer;}

    if(htmlAttribute.contains("fa-hand-peace"))
    {result = FontAwesomeIcons.handPeace;}

    if(htmlAttribute.contains("fa-trademark"))
    {result = FontAwesomeIcons.trademark;}

    if(htmlAttribute.contains("fa-registered"))
    {result = FontAwesomeIcons.registered;}

    if(htmlAttribute.contains("fa-creative-commons"))
    {result = FontAwesomeIcons.creativeCommons;}

    if(htmlAttribute.contains("fa-gg"))
    {result = FontAwesomeIcons.gg;}

    if(htmlAttribute.contains("fa-gg-circle"))
    {result = FontAwesomeIcons.gg;}

    if(htmlAttribute.contains("fa-tripadvisor"))
    {result = FontAwesomeIcons.tripadvisor;}

    if(htmlAttribute.contains("fa-odnoklassniki"))
    {result = FontAwesomeIcons.odnoklassniki;}

    if(htmlAttribute.contains("fa-odnoklassniki-square"))
    {result = FontAwesomeIcons.odnoklassnikiSquare;}

    if(htmlAttribute.contains("fa-get-pocket"))
    {result = FontAwesomeIcons.getPocket;}

    if(htmlAttribute.contains("fa-wikipedia-w"))
    {result = FontAwesomeIcons.wikipediaW;}

    if(htmlAttribute.contains("fa-safari"))
    {result = FontAwesomeIcons.safari;}

    if(htmlAttribute.contains("fa-chrome"))
    {result = FontAwesomeIcons.chrome;}

    if(htmlAttribute.contains("fa-firefox"))
    {result = FontAwesomeIcons.firefox;}

    if(htmlAttribute.contains("fa-opera"))
    {result = FontAwesomeIcons.opera;}

    if(htmlAttribute.contains("fa-internet-explorer"))
    {result = FontAwesomeIcons.internetExplorer;}

    if(htmlAttribute.contains("fa-tv"))
    {result = FontAwesomeIcons.tv;}

    if(htmlAttribute.contains("fa-contao"))
    {result = FontAwesomeIcons.contao;}

    if(htmlAttribute.contains("fa-500px"))
    {result = FontAwesomeIcons.fiveHundredPx;}

    if(htmlAttribute.contains("fa-amazon"))
    {result = FontAwesomeIcons.amazon;}

    if(htmlAttribute.contains("fa-calendar-plus"))
    {result = FontAwesomeIcons.calendarPlus;}

    if(htmlAttribute.contains("fa-calendar-minus"))
    {result = FontAwesomeIcons.calendarMinus;}

    if(htmlAttribute.contains("fa-calendar-times"))
    {result = FontAwesomeIcons.calendarTimes;}

    if(htmlAttribute.contains("fa-calendar-check"))
    {result = FontAwesomeIcons.calendarCheck;}

    if(htmlAttribute.contains("fa-industry"))
    {result = FontAwesomeIcons.industry;}

    if(htmlAttribute.contains("fa-map"))
    {result = FontAwesomeIcons.map;}

    if(htmlAttribute.contains("fa-map-pin"))
    {result = FontAwesomeIcons.mapPin;}

    if(htmlAttribute.contains("fa-map-signs"))
    {result = FontAwesomeIcons.mapSigns;}

    if(htmlAttribute.contains("fa-comments"))
    {result = FontAwesomeIcons.comments;}

    if(htmlAttribute.contains("fa-houzz"))
    {result = FontAwesomeIcons.houzz;}

    if(htmlAttribute.contains("fa-vimeo"))
    {result = FontAwesomeIcons.vimeo;}

    if(htmlAttribute.contains("fa-black-tie"))
    {result = FontAwesomeIcons.blackTie;}

    if(htmlAttribute.contains("fa-fonticons"))
    {result = FontAwesomeIcons.fonticons;}
    

    return result;
  }


  static IconData getCreditCardByName(String name){
    name = name != null ? name.toLowerCase() : "";
    IconData result = FontAwesomeIcons.creditCard;
    if(name == "visa"){
      result = FontAwesomeIcons.ccVisa;
    }
    if(name == "mastercard"){
      result = FontAwesomeIcons.ccMastercard;
    }
    if(name == "amex"){
      result = FontAwesomeIcons.ccAmex;
    }
    if(name == "paypal"){
      result = FontAwesomeIcons.ccPaypal;
    }
    return result;
  }


}
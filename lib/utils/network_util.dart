import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;

class NetworkUtil {
  // next three lines makes this class a Singleton
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  static final JsonDecoder _decoder = new JsonDecoder();
  static final  JsonEncoder _encoder = new JsonEncoder();

  static Future<dynamic> get(String url) {
    return http.get(url)
        .then((http.Response response) {
        final String res = response.body;
        final int statusCode = response.statusCode;

        if (statusCode < 200 || statusCode > 400 || json == null) {
          throw new Exception("Error while fetching data " + statusCode.toString());
        }
        return _decoder.convert(res);
    });
  }

  static Future<dynamic> post(String sUrl, {Map<String, String> oHeaders, oBody, Encoding encoding}) async {
//    
//    var mBody = _encoder.convert(oBody);
//    
    return http
        .post(sUrl, body: oBody, headers: oHeaders)
        .then((http.Response response) {
//      
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data " + statusCode.toString());
      }
      return _decoder.convert(res);
    })
        .catchError((Object error){
      print("INTERNAL ERROR POST " + error.toString());
      return error;
    });

  }



  static Future<dynamic> crilumaPost(String sUrl, {Map<String, String> oHeaders, String core}) async {
    String sBody = _makeBodyCrilumaRequest(core);
    print("request ->" + sBody);
    return http
      .post(sUrl, body: sBody, headers: oHeaders)
      .then((http.Response response) {
        final String res = response.body;
        final int statusCode = response.statusCode;

        if (statusCode < 200 || statusCode > 300 || json == null) {
          throw new Exception("Errore caricamento.\nCodice: " + statusCode.toString()+ "\nUrl: "+ sUrl);
        }
//        if(_decoder.convert(res)["d"] == "403"){
//          throw new Exception("Sessione scaduta chiudere e riaprire l'app ");
//        }
        return _decoder.convert(res);
    })
    .catchError((Object error){
      print("INTERNAL ERROR POST " + error.toString());
      return error;
    });

  }


  static String _makeBodyCrilumaRequest(String campi){


    return "{ \"pIngresso\": "
        "\'{ \"request\": ["
              "{ \"auth\": "
                  "{ \"username\": \"Pippo\", "
                    "\"password\": \"Pluto\", "
                   "\"Ip_Utente\": \"192.169.1.100\" }, "
              "\"core\": "
                "{ "
                    +campi+
                "} "
              "}]"
          "}' "
        "}";

  }

  static String makeBodyCrilumaRequestSenzaQuadrate(String campi){


    return "{ \"pIngresso\": "
        "\'{ \"request\": "
        "{ \"auth\": "
        "{ \"username\": \"Pippo\", "
        "\"password\": \"Pluto\", "
        "\"Ip_Utente\": \"192.169.1.100\" }, "
        "\"core\": "
        "{ "
        +campi+
        "} "
            "}"
            "}' "
            "}";

  }

}
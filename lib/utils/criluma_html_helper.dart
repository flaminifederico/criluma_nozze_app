


import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:xml/xml.dart' as xml;
import 'dart:developer';

import 'criluma_util.dart';
import 'fontawesome_helper.dart';


class CrilumaHtmlHelper {

  static String parseXML(String s){

    String res = "";
    try{
      var doc = xml.parse(s);
//      
      if(doc.descendants.length > 0 )
      res = doc.descendants.toList()[0].text;
//      doc.descendants.forEach(
//              (descendant) => {
//                debugger(),
//            res += descendant.text,
//          }) ;
//          :
//      res = doc.text;
    }
    catch(e){

    }


    return res;
  }

  static Widget parseHTML(String sHtml){

    return
      Html(
        useRichText: false,
        blockSpacing: 0.0,
        customRender: (dynamic node, children) {
          IconData icon;
          Widget otherWidgetAttr;
//
          if(node.attributes?.length > 0){
              node.attributes.values.forEach((attr) => {
                icon = FontAwesomeHelper.parseString(attr),
                otherWidgetAttr = parseNodeAttribute(node, attr)
              }
            );

            if(icon != null){
              return Icon(icon, color: blueCriluma.shade600, size: 15,);
            }
            else if (otherWidgetAttr != null){
              return otherWidgetAttr;
            }
//            else if(otherWidget != null){
//              Widget otherWidget  = iterateNodeFindLi(node);
//              
//              children;
//              counter++;
//              print("test");
//              return otherWidget;
//            }
          }

        },
        data: sHtml

    );


  }


  static Widget parseNodeAttribute(var node,  attr){
    if(attr.contains("h3")){
      return Text(node.nodes[0].data, style: TextStyle(fontSize: 18)); //prende il testo del primo nodo
    }
    else if(attr.contains("h5")){
      return Text(node.nodes[0].data, style: TextStyle(fontSize: 15)); //prende il testo del primo nodo
    }
    return null;
  }

//  static Widget iterateNodeFindLi(var node1){
//    Widget res;
//
//try{
//
//  if(node1.localName == "li"){
//    print("---------------------------------------------found--------------------------------------------------------");
//    res = Text("Found!");
//  }
//  else {
//    List<Widget> list = new List();
//    try{
//      print(node1.localName);
//
//      node1.nodes.forEach((node2) {
//        Widget el = iterateNodeFindLi(node2);
//       if(el != null){
//         list.add(el);
//       }
//      });
//    }catch(e){
//      print(e.toString());
//    }
//    if(list != null && list.length > 0){
//      res = Column( children: list );
//    }
//
//  }
//}
//catch(e){
//
//}
//
//
//    return res;
//  }

}


import 'package:flutter/material.dart';
import 'package:Criluma_Nozze/screens/signup_screen.dart';
import 'package:Criluma_Nozze/services/auth_service.dart';

class LoginScreen extends StatefulWidget {
  static final String id = 'login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  String _username = "", _password = "";

  _submit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      // Logging cpn Firebase da sostituire con WS Criluma
      AuthService.loginCriluma(_username, _password);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/cv.png"),
              fit: BoxFit.none,
              //alignment: const Alignment(0.0, -0.8),
              alignment: Alignment.topCenter,
            ),
          ),
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Accedi',
                style: TextStyle(
                  fontFamily: 'Billabong',
                  fontSize: 50.0,
                  color: Colors.blueAccent,
                ),
              ),
              Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 30.0,
                        vertical: 10.0,
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Nome Utente', labelStyle: TextStyle(color:Colors.blueAccent,) ),
                       // validator: (input,) => !input.contains('@')
                        //    ? 'Inserisci un indireizzo mail valido'
                       //     : null,
                        style: TextStyle(color:Colors.blueAccent),
                        onSaved: (input) => _username = input,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 30.0,
                        vertical: 10.0,
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(labelText: 'Password', labelStyle: TextStyle(color:Colors.blueAccent)),
                        //validator: (input) => input.length < 6
                        //    ? 'La Password deve contenere almeno 6 caratteri'
                         //   : null,
                        style: TextStyle(color:Colors.blueAccent),
                        onSaved: (input) => _password = input,
                        obscureText: true,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      width: 250.0,
                      child: FlatButton(
                        onPressed: _submit,
                        color: Colors.blue,
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          'Entra',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      width: 250.0,
                      child: FlatButton(
                        onPressed: () => Navigator.pushNamed(context, SignupScreen.id),
                        color: Colors.blue,
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          'Registrati',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

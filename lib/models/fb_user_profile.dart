

class FbUserProfile {

  String name;
  String firstName;
  String lastName;
  String email;
  String id;

  FbUserProfile.map(dynamic obj){
    name = obj["name"];
    firstName = obj["first_name"];
    lastName = obj["last_name"];
    email = obj["email"];
    id = obj["id"]; //facebook-id
  }
}
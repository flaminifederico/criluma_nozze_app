import 'dart:developer';

import 'package:flutter/material.dart';

class CrilumaUser {

  int idUtente;
  String email = "";
  String password = "";
  String codiceFiscale = "";
  String userName = "";
  String nome = "";
  String cognome = "";
  String dataNascita = "";
  String localitaNascita = "";
  String localitaResidenza = "";
  String indirizzo  = "";
  String telefono = "";
  String foto = "";
  bool disabilitato;
  bool isFromStorage = false;

  CrilumaUser();




  CrilumaUser.map(dynamic obj) {
    this.idUtente = obj["IdUtente"];
    this.email = obj["Email"];
    this.userName = obj["UserName"] != null ? obj["UserName"]  : "";
    this.password = obj["password"] != null ? obj["password"]  : "";
    this.nome = obj["Nome"];
    this.cognome = obj["Cognome"];
    this.dataNascita = obj["DataNascita"];
    this.localitaNascita = obj["LocalitaNascita"];
    this.localitaResidenza = obj["LocalitaResidenza"];
    this.indirizzo = obj["Indirzzo"]; //non cambiare l'errore è server side quindi per ora va lasciato così
    this.telefono = obj["Telefono"];
    this.foto = obj["Foto"];
    this.disabilitato = obj["Disabilitato"] != null && obj["Disabilitato"] == "true";
    this.isFromStorage = obj["isFromStorage"] != null && obj["isFromStorage"] == "true";
  }


  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["IdUtente"] = idUtente.toString();
    map["Email"] = email;
    map["UserName"] = userName;
    map["Nome"] = nome;
    map["Cognome"] = cognome;
    map["DataNascita"] = dataNascita;
    map["LocalitaNascita"] = localitaNascita;
    map["LocalitaResidenza"] = localitaResidenza;
    map["Indirzzo"] = indirizzo;
    map["Telefono"] = telefono;
    map["Foto"] = foto;
    map["Disabilitato"] = disabilitato;
    return map;
  }
}
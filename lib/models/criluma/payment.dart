import 'dart:developer';

class Payment {
  String idPreventivo = "";
  String userId = "";
  String paymentMethodId = "";
  String paymentIntentId = "";
  String clientSecret = "";
  String amount = "";
  String description = "";
  String last4digits = "";
  String brand = "";
  String monthYear = "";
  String status = "";
  String error = "";

  Payment();

  Payment.map(dynamic obj) {
//    
    idPreventivo = obj["idPreventivo"];
    userId = obj["userId"];
    paymentMethodId = obj["paymentMethodId"];
    paymentIntentId = obj["paymentIntentId"];
    clientSecret = obj["clientSecret"];
    amount = obj["amount"].toString();
    description = obj["description"];
    last4digits = obj["last4digits"];
    brand = obj["brand"];
    monthYear = obj["monthYear"];
    status = obj["status"];
    error = obj["error"];
  }
}

class CPaymentIntent {
  String clientSecret;
  String id;
  String paymentMethodId;
  String error;

  CPaymentIntent.map(dynamic obj){
    clientSecret = obj["client_secret"];
    id = obj["id"];
    paymentMethodId = obj["payment_method"];
    error = obj["StripeError"] != null ? obj["StripeError"]["message"] : "";
  }

}

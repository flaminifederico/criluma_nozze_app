import 'package:meta/meta.dart';

class Document {
  int id;
  DateTime dateCreated;
  String note;
  bool isDeleted;
  bool isRefused;
  bool isCompleted;
  int idParentDoc;

  Document(@required this.id, @required this.dateCreated);


  Document.map(dynamic obj) {
    this.id = obj["id"];
    this.dateCreated = obj["dateCreated"];
    this.note = obj["note"];
    this.isDeleted = obj["isDeleted"];
    this.isRefused = obj["isRefused"];
    this.isCompleted = obj["isCompleted"];
    this.idParentDoc = obj["idParentDoc"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["dateCreated"] = dateCreated;
    map["note"] = note;
    map["isDeleted"] = isDeleted;
    map["isRefused"] = isRefused;
    map["isCompleted"] = isCompleted;
    map["idParentDoc"] = idParentDoc;
    return map;
  }


}
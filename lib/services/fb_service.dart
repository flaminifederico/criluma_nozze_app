

import 'dart:convert';
import 'dart:developer';

import 'package:Criluma_Nozze/models/fb_user_profile.dart';
import 'package:Criluma_Nozze/utils/network_util.dart';

class FBService {

  static const GET_USER_PROFILE = "https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=";

  static Future<FbUserProfile> getUserProfile(String token) async {
    return NetworkUtil.get(GET_USER_PROFILE + token ,).then((dynamic res) {

      print(res.toString());
      if (res.toString() != null && res is Exception) {
        throw res;
      } else {
//        
//        var dec = _decoder.convert(res);
        return FbUserProfile.map(res);
      }
    }).catchError((Object error) {
      print(error.toString());
      throw error;
    });
  }
}
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:Criluma_Nozze/models/criluma/criluma_user.dart';
import 'package:Criluma_Nozze/models/user_data.dart';
import 'package:provider/provider.dart';
import 'package:Criluma_Nozze/utils/criluma_util.dart';
import 'package:Criluma_Nozze/utils/network_util.dart';
import 'package:http/http.dart' as http;

class AuthService {
  static final _auth = FirebaseAuth.instance;
  static final _firestore = Firestore.instance;
  static const PROFILE = BASE_URL + "/LogIn.aspx/getProfile";
  static const LOGIN_CRILUMA = BASE_URL + "/LogIn.aspx/getLogin";
  static const CHECK_EMAIL = BASE_URL + "/LogIn.aspx/CheckEmail";
  static const CHECK_USER_NAME = BASE_URL + "/LogIn.aspx/CheckUserName";
  static const GET_PROFILE_CRILUMA_USER = BASE_URL + "/LogIn.aspx/GetProfile";
  static const SET_PROFILE_CRILUMA_USER = BASE_URL + "/LogIn.aspx/SetProfile";
  static final JsonDecoder _decoder = new JsonDecoder();

  static Future<bool> loginCriluma(String username, String password) async {
    username = username == null ? "" : username;
    password = password == null ? "" : password;


    return NetworkUtil.crilumaPost(LOGIN_CRILUMA,
        oHeaders: {"Content-Type": "application/json"},
        core:  ' \"usernameutente\": \"'+username+'\",'
            ' \"passwordutente\": \"'+password+'\",'
    ).then((dynamic res) {
      print(res.toString());
//
      if (res.toString() != null && res is Exception) {
        throw res;
      } else {
        var step1 = _decoder.convert(res["d"]);
        if (step1["Stato"] == "OK") {
          return true;
        } else {
          throw Exception(step1["Descrizione"]);
        }
      }
    }).catchError((Object error) {
      print(error.toString());
      throw error;
    });
  }

  static Future<String> checkUsername(String username) async {

    //non è stato usato lo crilumaPost perchè in questa chiamata la request NON è un array
    String sBody = NetworkUtil.makeBodyCrilumaRequestSenzaQuadrate('\"UserName\": \"'+username+'\"');

    return http.post(CHECK_USER_NAME, body: sBody, headers: {"Content-Type": "application/json"})
        .then((http.Response response) {
//
      final String bodyRes = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 300 || json == null) {
        throw new Exception("Errore caricamento.\nCodice: " + statusCode.toString()+ "\nUrl: "+ CHECK_USER_NAME);
      }

      var res = _decoder.convert(bodyRes);
      print(res.toString());
      if (res.toString() != null && res is Exception) {
        throw res;
      } else {
        var step1 = _decoder.convert(res["d"]);
        if (step1["Stato"] == "OK") {
          return step1["Descrizione"]?.toString();
        } else {
          return "KO";
        }
      }
    }).catchError((var ex){
      print("INTERNAL ERROR POST " + ex.message);
      return ex.message;
    });

  }

  static Future<String> checkEmail(String email) async {

    //non è stato usato lo crilumaPost perchè in questa chiamata la request NON è un array
    String sBody = NetworkUtil.makeBodyCrilumaRequestSenzaQuadrate('\"Email\": \"'+email+'\"');



    return http.post(CHECK_EMAIL, body: sBody, headers: {"Content-Type": "application/json"})
        .then((http.Response response) {
//
      final String bodyRes = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 300 || json == null) {
        throw new Exception("Errore caricamento.\nCodice: " + statusCode.toString()+ "\nUrl: "+ CHECK_EMAIL);
      }

      var res = _decoder.convert(bodyRes);
      print(res.toString());
      if (res.toString() != null && res is Exception) {
        throw res;
      } else {
        var step1 = _decoder.convert(res["d"]);
        if (step1["Stato"] == "OK") {
          return step1["Descrizione"]?.toString();
        } else {
          return "KO";
        }
      }
    }).catchError((var ex){
      print("INTERNAL ERROR POST " + ex.message);
      return ex.message;
    });

  }

  static Future<CrilumaUser> getCrilumaUserProfile(String username) async {

    return NetworkUtil.crilumaPost(GET_PROFILE_CRILUMA_USER,
        oHeaders: {"Content-Type": "application/json"},
        core:
        ' \"UserName\": \"'+username+'\",'
            ' \"lang\": \"ita\"'
    ).then((dynamic res) {
      print(res.toString());

      if (res.toString() != null && res is Exception) {
        throw res;
      } else {
        var step1 = _decoder.convert(res["d"]);
        if (step1["Stato"] == "OK") {
          return CrilumaUser.map(step1["Descrizione"][0]);
        } else {
          throw new Exception(step1["Descrizione"]);
        }
      }
    }).catchError((Object error) {
      print(error.toString());
      throw error;
    });
  }



  static Future<CrilumaUser> setCrilumaUserProfile(CrilumaUser user) async {

    //non è stato usato lo crilumaPost perchè in questa chiamata la request NON è un array
    String sBody = NetworkUtil.makeBodyCrilumaRequestSenzaQuadrate(
        ' \"User\": {'
            '\"UserName\": \"'+user?.userName+'\",'
            '\"Password\": \"'+user?.password+'\",'
            '\"CodiceFiscale\": \"'+user?.codiceFiscale+'\",'
            '\"Cognome\": \"'+user?.cognome+'\",'
            '\"Nome\": \"'+user?.nome+'\",'
            '\"DataNascita\": \"'+user?.dataNascita+'\",'
            '\"LocalitaNascita\": \"'+user?.localitaNascita+'\",'
            '\"LocalitaResidenza\": \"'+user?.localitaResidenza+'\",'
            '\"Indirzzo\": \"'+user?.indirizzo+'\",' //non correggere il refuso 'Indirzzo' perchè  è server side il problema
            '\"Telefono\": \"'+user?.telefono+'\",'
            '\"Email\": \"'+user?.email+'\",'
            '\"Foto\": null,'
            '}'
    );

    return http.post(SET_PROFILE_CRILUMA_USER, body: sBody, headers: {"Content-Type": "application/json"})
        .then((http.Response response) {
//
      final String bodyRes = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 300 || json == null) {
        throw new Exception("Errore caricamento.\nCodice: " + statusCode.toString()+ "\nUrl: "+ SET_PROFILE_CRILUMA_USER);
      }

      var res = _decoder.convert(bodyRes);
      print(res.toString());
      if (res.toString() != null && res is Exception) {
        throw res;
      } else {
        var step1 = _decoder.convert(res["d"]);
        if (step1["Stato"] == "OK") {
          return user;
        } else {
          throw new Exception(step1["Descrizione"]);
        }
      }
    }).catchError((var ex){
      print("INTERNAL ERROR POST " + ex.message);
      return ex.message;
    });

  }













  static void signUpUser(
      BuildContext context, String name, String email, String password) async {
    try {
      AuthResult authResult = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      FirebaseUser signedInUser = authResult.user;
      if (signedInUser != null) {
        _firestore.collection('/users').document(signedInUser.uid).setData({
          'name': name,
          'email': email,
          'profileImageUrl': '',
        });
        Provider.of<UserData>(context).currentUserId = signedInUser.uid;
        Navigator.pop(context);
      }
    } catch (e) {
      print(e);
    }
  }

  static void logout() {
    _auth.signOut();
  }

  static void login(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      print(e);
    }
  }
}
